#include "rbt.h"

// ---------------------------------------
// Node class
// Default constructor
RBTNode::RBTNode() : Node() {
    color = BLACK;
}

// Constructor
RBTNode::RBTNode(int in) : Node(in) {
    color = BLACK;
}
// Destructor
RBTNode::~RBTNode() {
}

void RBTNode::add_color(Node_color c)
{
  color = c;
}

void RBTNode::print_color(ostream& to)
{
    if(color == RED) {
        to << "Red";
    } else if (color == BLACK) {
        to << "Black";
    } else {
        cerr << "ERR: invalid color" << endl;
    }
}
void RBTNode::print_info(ostream& to)
{
    to << get_key() << " ";
    print_color(to);
}

Node_color RBTNode::get_color()
{
  return color;
}
// ---------------------------------------


// ---------------------------------------
// RBT class
// Constructor and destructor
RBT::RBT() : BST()
{
  sentinel = new RBTNode(-1);
  root = sentinel;
}
RBT::~RBT()
{
    // re-using BST's inorder_free
    inorder_free(root, sentinel);
    // This makes sure that root is set to nullptr, so that the parent class's
    // constructor does not try to free the tree again
    root = nullptr;
    delete sentinel;
}

// Functions that are basically wrappers for the parent class functions
// minimum key in the BST
RBTNode* RBT::tree_min()
{
    // return (RBTNode*) get_min(root);
    // return (RBTNode*) BST::tree_min();
    return (RBTNode*) BST::get_min(root, sentinel);
}
// maximum key in the BST
RBTNode* RBT::tree_max()
{
    // return (RBTNode*) get_max(root);
    // return (RBTNode*) BST::tree_max();
    return (RBTNode*) BST::get_max(root, sentinel);
}

// Get successor of the given node
RBTNode* RBT::get_succ(RBTNode* in)
{
  return (RBTNode*) BST::get_succ((Node*) in, sentinel);
}

// Get predecessor of the given node
RBTNode* RBT::get_pred(RBTNode* in)
{
  return (RBTNode*) BST::get_pred((Node*) in, sentinel);
}

// Search the tree for a given key
RBTNode* RBT::tree_search(int search_key)
{
  return (RBTNode*) BST::tree_search(search_key, sentinel);
}

void RBT::walk(ostream& to)
{
  BST::inorder_walk(root, to, sentinel);
}



// New functions to RBT
// right rotate
void RBT::right_rotate(RBTNode* y)
{
    /* TODO */

    Node* lChild = y->get_left();
    Node* gRChild = lChild->get_right();
    Node* parent = y->get_parent();

    bool isLChild = false;
    if(parent != sentinel) {
      if(parent->get_left() == y) {
        if(parent->get_right() != y) {
          isLChild = true;
        }
      }
    }

    y->add_right(gRChild);

    if(gRChild != nullptr)
      gRChild->add_parent(y);

    lChild->add_parent(parent);

    if(parent == sentinel)
      root = lChild;
    else if(isLChild)
      parent->add_right(lChild);
    else
      parent->add_left(lChild);

    lChild->add_right(y);
    y->add_parent(lChild);


}
// Left rotate
void RBT::left_rotate(RBTNode* x)
{
    /* TODO */

    Node* rChild = x->get_right();
    Node* glChild = rChild->get_left();
    Node* parent = x->get_parent();
    Node* thisNode = x;

    // bool isLChild = false;
    // if(parent != sentinel) {
    //   if(parent->get_left() == x) {
    //     if(parent->get_right() != x) {
    //       isLChild = true;
    //     }
    //   }
    // }

    bool isLChild = thisNode->isLeftChild();

    x->add_right(glChild);

    if(glChild != nullptr)
      glChild->add_parent(x);

    rChild->add_parent(parent);

    if(parent == sentinel)
      root = rChild;
    else if(isLChild)
      parent->add_left(rChild);
    else
      parent->add_right(rChild);

    rChild->add_left(x);
    x->add_parent(rChild);


    // RBTNode* rChild = (RBTNode*) &rightChild;
    // RBTNode* glChild = (RBTNode*) &gleftChild;
    // RBTNode* parent = (RBTNode*) &nodeParent;

}
void RBT::rb_insert_fixup(RBTNode* in)
{
    /* TODO */

    RBTNode* currentNode = in;
    Node_color red = RED;
    Node_color black = BLACK;

    Node* gParent;
    Node* uncle;
    Node* parent = currentNode->get_parent();

    RBTNode* rbParent;
    RBTNode* rbUncle;
    RBTNode* rbGParent;

    rbParent = (RBTNode*) parent;
    while((parent != nullptr) && (currentNode->get_color() == RED) && (rbParent->get_color() == RED)) {

      parent = currentNode->get_parent();
      rbParent = (RBTNode*) parent;
      gParent = parent->get_parent();
      rbGParent = (RBTNode*) gParent;

      if(parent->isLeftChild()) {
        uncle = gParent->get_right();
        RBTNode* rbUncle = (RBTNode*) uncle;
        if((uncle != nullptr) && (rbUncle->get_color() == RED)) {
          rbParent->add_color(black);
          rbUncle->add_color(black);
          rbGParent->add_color(red);
          currentNode = rbGParent;
        }
        else {
          if(currentNode->isRightChild()) {
            rbParent = (RBTNode*) parent;
            currentNode = rbParent;
            gParent = parent->get_parent();
            rbGParent = (RBTNode*) gParent;
            parent = currentNode->get_parent();
            rbParent = (RBTNode*) parent;
            this->left_rotate(currentNode);
          }
          rbParent->add_color(black);
          rbGParent->add_color(red);
          this->right_rotate(rbGParent);
        }
      }
      else {
        uncle = gParent->get_left();
        rbUncle = (RBTNode*) uncle;
        rbParent = (RBTNode*) parent;
        rbGParent = (RBTNode*) gParent;
        if((uncle != nullptr) && (rbUncle->get_color() == RED)) {
          rbParent->add_color(black);
          rbUncle->add_color(black);
          rbGParent->add_color(red);
          currentNode = rbGParent;
        }
        else {
          if(currentNode->isLeftChild()) {
            currentNode = rbParent;
            gParent = parent->get_parent();
            parent = currentNode->get_parent();
            this->right_rotate(currentNode);
          }
          rbParent = (RBTNode*) parent;
          rbParent->add_color(black);
          rbGParent = (RBTNode*) gParent;
          rbGParent->add_color(red);
          this->left_rotate(rbGParent);
        }
      }
    }
    RBTNode* rbRoot = (RBTNode*) root;
    rbRoot->add_color(black);
}

void RBT::rb_insert_node(RBTNode* in)
{
    /* TODO */

    //BST* bst = (BST*) this;
    this->insert_node(in);
    //RBT* rbt = (RBT*) &bst;
    this->rb_insert_fixup(in);


}

void RBT::rb_delete_fixup(RBTNode* in)
{
    /* TODO */
}

void RBT::rb_delete_node(RBTNode* out)
{
    /* TODO */
}

// ---------------------------------------
