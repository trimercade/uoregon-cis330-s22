#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#define MAX_NAME_LENGTH 100

struct student_record {
  char* name;
  unsigned int age;
};

int main(int argc, char** argv)
{
  // This is a single struct
  struct student_record my_record;
  my_record.name = (char*) malloc(sizeof(char) * MAX_NAME_LENGTH);
  strcpy(my_record.name, "Jane Smith");
  my_record.age = 10;
  printf("%s is %d years old\n", my_record.name, my_record.age);

  // This is an array of structs
  struct student_record* my_record_array;
  my_record_array = (struct student_record*) malloc(sizeof(struct student_record) * 100);
  for(int i = 0; i < 10; i++) {
    my_record_array[i].name = (char*) malloc(sizeof(char) * MAX_NAME_LENGTH);
    my_record_array[i].age = i;
    strcpy(my_record_array[i].name, "Jane Smith");
  }
  for(int i = 0; i < 10; i++) {
    printf("%s is %d years old\n", my_record_array[i].name, 
                                   my_record_array[i].age);
  }

  // a 2-D array of structs vs. a 1-D array of struct*
  struct student_record** my_record_2d_array;
  my_record_2d_array = (struct student_record**) malloc(sizeof(struct student_record*) * 100);
  for(int i = 0; i < 100; i++) {
    // Each element of this array is pointing to a single struct
    // If we were to allocate more than 1 struct student_record, then this would
    // become a 2-D array
    // Since each element of my_record_2d_array[i] is a pointer, -> must be
    // used to access its members
    my_record_2d_array[i] = (struct student_record*) malloc(sizeof(struct student_record) * 1);
    my_record_2d_array[i]->name = (char*) malloc(sizeof(char) * MAX_NAME_LENGTH);
    my_record_2d_array[i]->age = i;
  }
  

  // Don't forget to free
  for(int i = 0; i < 100; i++) {
    free(my_record_2d_array[i]->name);
    free(my_record_2d_array[i]);
  } 
  free(my_record_2d_array);
  for(int i = 0; i < 10; i++) {
    free(my_record_array[i].name);
  }
  free(my_record_array);
  free(my_record.name);


  // Creating a 2-D array
  int rows = 10;
  int cols = 10;
  int cnt = 0;
  int** d2_array = (int**) malloc(sizeof(int*) * rows);
  for(int i = 0; i < rows; i++) {
    d2_array[i] = (int*) malloc(sizeof(int) * cols);
    for(int j = 0; j < cols; j++) {
      // initializing the array with a sequence
      d2_array[i][j] = cnt++;
    }
  }
  int indx = 0;
  int* d1_array = (int*) malloc(sizeof(int) * rows * cols);
  for(int i = 0; i < rows; i++) {
    for(int j = 0; j < cols; j++) {
      d1_array[indx++] = d2_array[i][j];
    }

  }

  // Using pointers to allocate one large memory space
  // You can use malloc to create memory, but if you are mallocing and freeing
  // small amounts of memory repeatedly, this can slow down your code, because
  // allocating and freeing memory is not cheap
  int* tmp1 = (int*) malloc(sizeof(int) * 128);
  int* tmp2 = (int*) malloc(sizeof(int) * 256);

  // Instead, you can create one large memory area using a single malloc,
  // and then have pointers point to different starting positions, and
  // as long as you don't go beyond the space you had intended to use, your
  // code should work as intended.
  int* scratch = (int*) malloc(sizeof(int) * (128 + 256));
  int* tmpa = &(scratch[0]);
  int* tmpb = scratch + 128;


  // When freeing a multi-dimensional array, free in reverse order to how it
  // was allocated
  free(scratch);
  free(tmp1);
  free(tmp2);
  free(d1_array);
  for(int i = 0; i < rows; i++) {
    free(d2_array[i]);
  }
  free(d2_array);
  return 0;
}
