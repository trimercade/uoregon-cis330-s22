#include <time.h> 
#include <stdio.h>
#include <stdlib.h>
#include "mytimer.h"

void get_nstime(mytime_t* ts)
{
  int res = clock_gettime(CLOCK_MONOTONIC, ts);
  if(res == -1) {
    fprintf(stderr, "Error measuring time\n");
    exit(EXIT_FAILURE);
  }
}


double elapsed_nstime(mytime_t ts)
{
  mytime_t stop;
  int res = clock_gettime(CLOCK_MONOTONIC, &stop);
  if(res == -1) {
    fprintf(stderr, "Error measuring time\n");
    exit(EXIT_FAILURE);
  }
  double elapsed = (stop.tv_sec - ts.tv_sec) + 
                   (1.0 * (stop.tv_nsec - ts.tv_nsec) / BIL);
  return elapsed; 
}  
  
