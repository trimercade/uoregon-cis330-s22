#ifndef MYTIMER_H
#define MYTIMER_H
#define BIL 1000000000L
typedef struct timespec mytime_t;
void get_nstime(mytime_t* ts);
double elapsed_nstime(mytime_t ts);
#endif

