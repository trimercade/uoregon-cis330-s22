#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include <vector>
#include <algorithm>
#include <getopt.h>

using namespace std;

class A {
private:
  int a;
public:
  A() { a = 10; }
  void set_a(int x) { a = x; }
  int get() { return a; }
  virtual void print() { cout << "argh1: " << a << endl; }
};

class B : public A {
private:
  int b;
public:
  B() : A() { b = 20; }
  void set_b(int x) { b = x; }
  int get() { return b; }
  void print() { cout << "argh2: " << b << endl; }
};


int main(int argc, char** argv)
{
  A** list = new A*[10];
  for(int i = 0; i < 10; i++) {
    list[i] = new B(); // upcasting B to A
    // list[i] = new A();
  }
  cout << list[0]->get() << endl;
  cout << ((B*) list[0])->get() << endl; // downcasting A to B (if init with A)
  list[0]->print();

  // list[0]->set_b(100);
  ((B*) list[0])->set_b(100);
  list[0]->print();

  return 0;
}
