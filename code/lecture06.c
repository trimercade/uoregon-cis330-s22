#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <time.h>
#include "mytimer.h"

/* Definition of struct timespace
struct timespec {
  time_t   tv_sec;        // seconds 
  long     tv_nsec;       // nanoseconds
};
 */

int main(int argc, char** argv) 
{
  // Example for measuring time

  #if 1
  struct timespec start;
  struct timespec stop;

  int res_start = clock_gettime(CLOCK_MONOTONIC, &start); 
  if(res_start == -1) {
    fprintf(stderr, "Error measuring time\n");
    exit(EXIT_FAILURE);
  }

  // sleep for 2 seconds
  sleep(2); // from unistd.h

  int res_stop = clock_gettime(CLOCK_MONOTONIC, &stop); 
  if(res_stop == -1) {
    fprintf(stderr, "Error measuring time\n");
    exit(EXIT_FAILURE);
  }


  printf("Start: %10ld.%09ld\n", (long) start.tv_sec, start.tv_nsec);
  printf("Stop: %10ld.%09ld\n", (long) stop.tv_sec, stop.tv_nsec);

  // One way to print the elapsed time
  printf("Elapsed: %10ld.%09ld\n", (long) stop.tv_sec - start.tv_sec,
          stop.tv_nsec - start.tv_nsec);

  // Another way to print the elapsed time
  double elapsed = (stop.tv_sec - start.tv_sec) + 
                   (1.0 * (stop.tv_nsec - start.tv_nsec) / BIL);
  printf("Elapsed: %.10f\n", elapsed);
  #else
  mytime_t start;
  get_nstime(&start);
  sleep(2);
  double elapsed = elapsed_nstime(start); 
  printf("%.9f\n", elapsed);
  
  #endif



  return 0;
}
