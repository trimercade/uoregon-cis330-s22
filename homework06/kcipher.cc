#include <string>
#include <iostream>
#include <vector>
#include "kcipher.h"




/* Helper function definitions
 */

// -------------------------------------------------------
// Running Key Cipher implementation
// -------------------------------------------------------
KCipher::KCipher() {
  keyword = "a";
}

KCipher::KCipher(string first_page) {
  book.push_back(first_page);
}

KCipher::~KCipher() {

}

void KCipher::add_key(string page) {
  book.push_back(page);
}

void KCipher::set_id(int in_page = 0) {
  pageNum = in_page;
}

string KCipher::encrypt(string raw) {
  string retStr;
  cout << "Encrypting...";

  int upperFlag = 0;
  retStr = "";
  string shiftedAlpha;
  string repeatingKey;
  int offset;

  keyword = book[pageNum];
  repeatingKey = setRepeatingKey(raw);

  for(int i = 0; raw[i]; i++) {
    if(raw[i] == ' ') {
      retStr += " ";
    }
    else {
      if(isupper(raw[i])) {
        char lower = tolower(raw[i]);
        raw[i] = lower;
        upperFlag = 1;
      }

      offset = findOffset(raw[i]);
      shiftedAlpha = setShiftedAlpha(offset);

      offset = findOffset(repeatingKey[i]);
      if(upperFlag) {
        retStr += toupper(shiftedAlpha[offset]);
        upperFlag = 0;
      }
      else {
        retStr += shiftedAlpha[offset];
      }


    }
  }
  cout << "Done" << endl;
  //cout << "Enc: " << retStr << "\n";
  return retStr;
}

string KCipher::decrypt(string enc) {
  string retStr;
  cout << "Decrypting...";
  // TODO: Finish this function
  int upperFlag = 0;
  retStr = "";
  string shiftedAlpha;
  string repeatingKey;
  int encodedOffset;
  int keyOffset;

  keyword = book[pageNum];
  repeatingKey = setRepeatingKey(enc);

  for(int i = 0; enc[i]; i++) {

    if(enc[i] == ' ') {
       retStr += " ";
     }

     else {
       if(isupper(enc[i])) {
         char lower = tolower(enc[i]);
         enc[i] = lower;
         upperFlag = 1;
       }

       keyOffset = findOffset(repeatingKey[i]);
       shiftedAlpha = setShiftedAlpha(keyOffset);

       encodedOffset = findOffset(enc[i]);
       encodedOffset -= (2 * keyOffset);
       while(encodedOffset < 0) {
         encodedOffset = 26 + encodedOffset;
       }
       if(upperFlag) {
         retStr += toupper(shiftedAlpha[encodedOffset]);
         upperFlag = 0;
       }
       else {
         retStr += shiftedAlpha[(encodedOffset)];
       }

    }

  }

  cout << "Done" << endl;
  //cout << "Dec: " << retStr << "\n";
  return retStr;

}
