#ifndef VCIPHER_H_
#define VCIPHER_H_
#include "cipher.h"
#include "kcipher.h"

using namespace std;

/* A class that implements a
   Vigenere cipher class. It
   inherts KCipher */
// TODO: Implement this class

class VCipher : public Cipher {
protected:
    string keyword;
public:
  VCipher();
  VCipher(string);
  ~VCipher();
  string encrypt(string raw);
  string decrypt(string enc);
  string setShiftedAlpha(int);
  int findOffset(char);
  string setRepeatingKey(string);
  
};


#endif
