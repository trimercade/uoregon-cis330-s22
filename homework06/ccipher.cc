#include <string>
#include <iostream>
#include <algorithm>
#include "ccipher.h"


// -------------------------------------------------------
//Caesar Cipher implementation
struct CCipher::CipherCheshire {
    string cipher_alpha;
};

// -------------------------------------------------------
CCipher::CCipher() {
  smile = new CipherCheshire;
  smile->cipher_alpha = "abcdefghijklmnopqrstuvwxyz";
  //Cipher my_cipher(setShiftedAlpha(0));
}

CCipher::CCipher(int offset) {
  smile = new CipherCheshire;
  smile->cipher_alpha = setShiftedAlpha(offset);
  //Cipher my_cipher(setShiftedAlpha(offset));
}

CCipher::~CCipher() {
  delete smile;
}

string CCipher::setShiftedAlpha(int offset) {
  string ref = "abcdefghijklmnopqrstuvwxyz";
  string shifted = "";

  for(int i = 0; ref[i]; i++) {
      shifted += ref[((i + offset)%26)];
    }
  cout << "shifted: " << shifted << "\n";

  return shifted;
}

string CCipher::encrypt(string raw) {

  string retStr;
  cout << "Encrypting...";

  int upperFlag = 0;
  retStr = "";
  string reference = "abcdefghijklmnopqrstuvwxyz";

  for(int i = 0; raw[i]; i++) {
    //if the character is a space, append a space to the output string
    if(raw[i] == ' ') {
      retStr += " ";
    }
    //else if the character is not a space, iterate through the reference alpha string
    //if the character is equal to a reference character, append the cipher character
    //at the reference character index to the output string
    else {
      if(isupper(raw[i])) {
        char lower = tolower(raw[i]);
        raw[i] = lower;
        upperFlag = 1;
      }

      for(int j = 0; reference[j]; j++) {
        if(raw[i] == reference[j]) {
          if(upperFlag) {
            retStr += toupper(smile->cipher_alpha[j]);
            upperFlag = 0;
          }
          else
            retStr += smile->cipher_alpha[j];
        }
      }
    }
  }
  cout << "Done" << endl;
  //cout << "Enc: " << retStr << "\n";
  return retStr;
}

string CCipher::decrypt(string enc)
{
    string retStr;
    cout << "Decrypting...";
    // TODO: Finish this function
    //cout << "Enc: " << enc << "\n";
    int upperFlag = 0;
    retStr = "";
    string reference = "abcdefghijklmnopqrstuvwxyz";

    for(int i = 0; enc[i]; i++) {

      if(enc[i] == ' ') {
        retStr += " ";
      }

      else {
        if(isupper(enc[i])) {
          char lower = tolower(enc[i]);
          enc[i] = lower;
          upperFlag = 1;
        }

        for(int j = 0; smile->cipher_alpha[j]; j++) {
          if(enc[i] == smile->cipher_alpha[j]) {
            if(upperFlag) {
              retStr += toupper(reference[j]);
              upperFlag = 0;
            }
            else
              retStr += reference[j];
          }
        }


      }

    }

    cout << "Done" << endl;
    //cout << "Dec: " << retStr << "\n";
    return retStr;

}

// Rotates the input string in_str by rot positions
void rotate_string(string& in_str, int rot)
{
    // TODO: You will likely need this function. Implement it.
    string ref = "abcdefghijklmnopqrstuvwxyz";
    string shifted = "";

    for(int i = 0; ref[i]; i++) {
      if((i + rot) < 27) {
        shifted += ref[(i + rot)];
      }
      else {
        shifted += ref[((i + rot)%26) - 1];
      }
    }

}
