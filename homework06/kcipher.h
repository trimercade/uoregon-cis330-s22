#ifndef KCIPHER_H_
#define KCIPHER_H_
#include "cipher.h"
#include "ccipher.h"
#include "vcipher.h"

using namespace std;

const unsigned int MAX_LENGTH = 100;

/* A class that implements a
   Running key cipher class. It
   inherts class Cipher */
// TODO: Implement this function

class KCipher : public VCipher {
protected:
  vector<string> book;
  int pageNum;
public:
  KCipher();
  KCipher(string);
  ~KCipher();
  void add_key(string);
  void set_id(int);
  string encrypt(string raw);
  string decrypt(string enc);

};
#endif
