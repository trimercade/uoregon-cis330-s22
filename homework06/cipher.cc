#include "cipher.h"
#include <string>
#include <cctype>
#include <cstring>

/* Cheshire smile implementation.
   It only contains the cipher alphabet
 */
struct Cipher::CipherCheshire {
    string cipher_alpha;
};

/* This function checks the cipher alphabet
   to make sure it's valid
 */
bool is_valid_alpha(string alpha);


// -------------------------------------------------------
// Cipher implementation
/* Default constructor
   This will actually not encrypt the input text
   because it's using the unscrambled alphabet
 */
Cipher::Cipher()
{
    // TODO: Implement this default constructor
    smile = new CipherCheshire;
    smile->cipher_alpha = "abcdefghijklmnopqrstuvwxyz";
    //smile->cipher_alpha = "zyxwvutsrqponmlkjihgfedcba";
}

/* This constructor initiates the object with a
   input cipher key
 */
Cipher::Cipher(string cipher_alpha)
{
    // TODO: Implement this constructor
    smile = new CipherCheshire;
    smile->cipher_alpha = cipher_alpha;
}

/* Destructor
 */
Cipher::~Cipher()
{
    // TODO: Implement this constructor
    delete smile;
}

/* This member function encrypts the input text
   using the intialized cipher key
 */
string Cipher::encrypt(string raw)
{
    //raw = "I am in tHe HoUse with the dog";
    //cout << "Raw: " << raw << "\n";
    string retStr;
    cout << "Encrypting...";
    // TODO: Finish this function
    int upperFlag = 0;
    retStr = "";
    string reference = "abcdefghijklmnopqrstuvwxyz";

    //iterate through the raw input string
    for(int i = 0; raw[i]; i++) {
      //if the character is a space, append a space to the output string
      if(raw[i] == ' ') {
        retStr += " ";
      }
      //else if the character is not a space, iterate through the reference alpha string
      //if the character is equal to a reference character, append the cipher character
      //at the reference character index to the output string
      else {
        if(isupper(raw[i])) {
          char lower = tolower(raw[i]);
          raw[i] = lower;
          upperFlag = 1;
        }

        for(int j = 0; reference[j]; j++) {
          if(raw[i] == reference[j]) {
            if(upperFlag) {
              retStr += toupper(smile->cipher_alpha[j]);
              upperFlag = 0;
            }
            else
              retStr += smile->cipher_alpha[j];
          }
        }
      }
    }
    cout << "Done" << endl;
    //cout << "Enc: " << retStr << "\n";
    return retStr;
}


/* This member function decrypts the input text
   using the intialized cipher key
 */
string Cipher::decrypt(string enc)
{
    string retStr;
    cout << "Decrypting...";
    // TODO: Finish this function
    //cout << "Enc: " << enc << "\n";
    int upperFlag = 0;
    retStr = "";
    string reference = "abcdefghijklmnopqrstuvwxyz";

    for(int i = 0; enc[i]; i++) {

      if(enc[i] == ' ') {
        retStr += " ";
      }

      else {
        if(isupper(enc[i])) {
          char lower = tolower(enc[i]);
          enc[i] = lower;
          upperFlag = 1;
        }

        for(int j = 0; smile->cipher_alpha[j]; j++) {
          if(enc[i] == smile->cipher_alpha[j]) {
            if(upperFlag) {
              retStr += toupper(reference[j]);
              upperFlag = 0;
            }
            else
              retStr += reference[j];
          }
        }


      }

    }

    cout << "Done" << endl;
    //cout << "Dec: " << retStr << "\n";
    return retStr;

}
// -------------------------------------------------------


//  Helper functions
/* Find the character c's position in the cipher alphabet/key
 */
unsigned int find_pos(string alpha, char c)
{
    unsigned int pos = 0;
    for(int i = 0; alpha[i]; i++) {
      if(i == alpha[i]) {
        pos = i;
      }
    }
    // TODO: You will likely need this function. Finish it.

    return pos;
}

/* Make sure the cipher alphabet is valid -
   a) it must contain every letter in the alphabet
   b) it must contain only one of each letter
   c) it must be all lower case letters
   ALL of the above conditions must be met for the text to be a valid
   cipher alphabet.
 */
bool is_valid_alpha(string alpha)
{
    bool is_valid = true;
    if(alpha.size() != ALPHABET_SIZE) {
        is_valid = false;
    } else {
        unsigned int letter_exists[ALPHABET_SIZE];
        for(unsigned int i = 0; i < ALPHABET_SIZE; i++) {
            letter_exists[i] = 0;
        }
        for(unsigned int i = 0; i < alpha.size(); i++) {
            char c = alpha[i];
            if(!((c >= 'a') && (c <= 'z'))) {
                is_valid = false;
            } else {
                letter_exists[(c - 'a')]++;
            }
        }
        for(unsigned int i = 0; i < ALPHABET_SIZE; i++) {
            if(letter_exists[i] != 1) {
                is_valid = false;
            }
        }
    }

    return is_valid;
}
