#include <string>
#include <iostream>
#include <vector>
#include <algorithm>
#include "kcipher.h"
#include "vcipher.h"


// -------------------------------------------------------
// Running Key Cipher implementation
// -------------------------------------------------------

VCipher::VCipher() {
  keyword = "a";
}

VCipher::VCipher(string in_keyword) {
  keyword = in_keyword;
}

VCipher::~VCipher() {

}

string VCipher::setShiftedAlpha(int offset) {
  string ref = "abcdefghijklmnopqrstuvwxyz";
  string shifted = "";

  for(int i = 0; ref[i]; i++) {
      shifted += ref[((i + offset)%26)];
    }

  return shifted;
}

int VCipher::findOffset(char in_char) {
  string reference = "abcdefghijklmnopqrstuvwxyz";

  int offset = -1;

  for(int i = 0; reference[i]; i++) {
    if(in_char == reference[i]) {
      offset = i;
    }
  }
  return offset;
}

string VCipher::setRepeatingKey(string raw) {
  string repeat = "";
  int sizekey = 0;
  for(int i = 0; keyword[i]; i++) {
    sizekey++;
  }
  int keyindex = 0;
  for(int i = 0; raw[i]; i++) {
    if(raw[i] == ' ') {
      repeat += '-';
    }
    else {
      if(keyindex < sizekey) {
        repeat += keyword[keyindex];
      }
      keyindex++;
      if(keyindex > sizekey) {
        repeat += keyword[0];
        keyindex = 0;
      }
    }

  }
  return repeat;
}

string VCipher::encrypt(string raw) {

  string retStr;
  cout << "Encrypting...";

  int upperFlag = 0;
  retStr = "";
  string shiftedAlpha;
  string repeatingKey;
  int offset;

  repeatingKey = setRepeatingKey(raw);

  for(int i = 0; raw[i]; i++) {
    if(raw[i] == ' ') {
      retStr += " ";
    }
    else {
      if(isupper(raw[i])) {
        char lower = tolower(raw[i]);
        raw[i] = lower;
        upperFlag = 1;
      }

      offset = findOffset(raw[i]);
      shiftedAlpha = setShiftedAlpha(offset);

      offset = findOffset(repeatingKey[i]);
      if(upperFlag) {
        retStr += toupper(shiftedAlpha[offset]);
        upperFlag = 0;
      }
      else {
        retStr += shiftedAlpha[offset];
      }


    }
  }
  cout << "Done" << endl;
  //cout << "Enc: " << retStr << "\n";
  return retStr;
}

string VCipher::decrypt(string enc)
{
    string retStr;
    cout << "Decrypting...";
    // TODO: Finish this function
    int upperFlag = 0;
    retStr = "";
    string shiftedAlpha;
    string repeatingKey;
    int encodedOffset;
    int keyOffset;

    repeatingKey = setRepeatingKey(enc);

    for(int i = 0; enc[i]; i++) {

      if(enc[i] == ' ') {
         retStr += " ";
       }

       else {
         if(isupper(enc[i])) {
           char lower = tolower(enc[i]);
           enc[i] = lower;
           upperFlag = 1;
         }

         keyOffset = findOffset(repeatingKey[i]);
         shiftedAlpha = setShiftedAlpha(keyOffset);

         encodedOffset = findOffset(enc[i]);
         encodedOffset -= (2 * keyOffset);
         while(encodedOffset < 0) {
           encodedOffset = 26 + encodedOffset;
         }
         if(upperFlag) {
           retStr += toupper(shiftedAlpha[encodedOffset]);
           upperFlag = 0;
         }
         else {
           retStr += shiftedAlpha[(encodedOffset)];
         }

      }

    }

    cout << "Done" << endl;
    //cout << "Dec: " << retStr << "\n";
    return retStr;

}
